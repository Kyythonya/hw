﻿using System;
using System.IO;
using System.Net;
using System.Windows;

namespace DownloadFileWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private async void DownloadButtonClick(object sender, RoutedEventArgs e)
        {
            using (var client = new WebClient())
            {
                await client.DownloadFileTaskAsync(urlTextBox.Text, $"{Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)}\\{fileNameTextBox.Text}");
            }
        }
    }
}
